package fr.philippine;
import java.util.Scanner;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class Fibonacci {

    private static Logger logger = Logger.getLogger(Fibonacci.class);


    public static void main(String args[]) {

        System.out.println("Saisissez un chiffre : ");
        int number = new Scanner(System.in).nextInt();
        logger.debug("Nombre entré : " + number);

        for (int i = 1; i <= number; i++) {
            System.out.println(fibonacciRecusion(i) + " ");
            logger.debug(fibonacciRecusion(i) + "\t");
        }


        BasicConfigurator.configure();





    }


    private static int fibonacciRecusion(int number) {
        if (number == 1 || number == 2) {
            return 1;
        }

        return fibonacciRecusion(number - 1) + fibonacciRecusion(number - 2);
    }



}